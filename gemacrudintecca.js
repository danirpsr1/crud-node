'use strict'

const CONFIG = require('./config')[process.env.NODE_ENV || 'desarrollo'];

process.env.DEBUG= [CONFIG.DEBUG || 'gemacrudintecca:*'];

const debug = require('debug')('gemacrudintecca:inicio');

const SISTEMA = require('./app/modbasico/basico/sistema.js');

const mongoose = require('./app/config/mongoose');

mongoose.cargarBd();

const middleware = require('./app/config/middleware')

let app = middleware()

app.listen(CONFIG.PORT)

debug(`Entorno: ${[process.env.NODE_ENV]}\n
VERSION: ${CONFIG.VERSION}\n
MONGO_DB:${CONFIG.MONGO.db}\n
MONGO_PORT:${CONFIG.MONGO.port}\n
APP_LISTEN_PORT:${CONFIG.PORT}\n
`);

debug('process.cwd()',process.cwd());

// versiones básicas instaladas
//const COMANDOS = ["node --version", "npm --version", "npm list express", "npm list mongodb", "npm list mongoose", "mongod --version"];

// const COMANDOS = ["node --version", "npm --version", "npm list express", "npm list mongodb", "npm list mongoose"];

// SISTEMA.runComandos(COMANDOS)
// 	.then(res => debug(res))
//     .catch(err => debug(err))
