Aplicativo semilla:

# Empezar

- Clonar el proyecto

```js
git clone https://gitlab.com/intecca-iot-practicas-2021/semillanodecrud.git
```

- Instalar
```js
npm install
```

# Configurar 
- Base de datos.

Archivo config.js
```js
desarrollo -> 
  --> port: PUERTO EN EL QUE EJECUTAS NODE
  --> MONGO.db: NOMBRE DE LA BASE DE DATOS A UTILIZAR 
  --> MONGO.host: IP DEL SERVIDOR DE MONGO
  --> MONGO.port: PUERTO DEL SERVIDOR DE MONGO
```
- Debug
    - gemacrudintecca: Denominación de nuestra aplicación


## Ejecutar
```js
npm run start
```
- Desde Visual Studio Code

Utilizar el archivo .vscode/launch.js

Run --> Starting Debug (F5)


# Herramientas:
- morgan: loger de accesos
- rotating-file-stream: rotar ficheros logs
- serve-index: para los logs


Aplicativo semilla:

# Empezar

- Clonar el proyecto

```js
git clone https://gitlab.com/intecca-iot-practicas-2021/semillanodecrud.git
```

- Instalar
```js
npm install
```

# Configurar 
- Base de datos.

Archivo config.js
```js
desarrollo -> 
  --> port: PUERTO EN EL QUE EJECUTAS NODE
  --> MONGO.db: NOMBRE DE LA BASE DE DATOS A UTILIZAR 
  --> MONGO.host: IP DEL SERVIDOR DE MONGO
  --> MONGO.port: PUERTO DEL SERVIDOR DE MONGO
```
- Debug
    - gemacrudintecca: Denominación de nuestra aplicación


## Ejecutar
```js
npm run start
```
- Desde Visual Studio Code

Utilizar el archivo .vscode/launch.js

Run --> Starting Debug (F5)


# Herramientas:
- morgan: loger de accesos
- rotating-file-stream: rotar ficheros logs
- serve-index: para los logs

# Cómo usar:

## Crear llamadas desde Postman:
- GET `http://localhost:PUERTO/api/sistemasolar/planetas/`
- GET `http://localhost:PUERTO/api/sistemasolar/planetas/identificador` siendo identificador el _id del elemento en MongoDB
- POST `http://localhost:PUERTO/api/sistemasolar/planetas/` con el body en formato json:
  ```json 
    {
        "nombre": "loquesea",
        "descripcion": "loquesea"
    }
  ```
- POST `http://localhost:PUERTO/api/sistemasolar/planetas/query` para filtrar, con el body en formato json:
```json 
    {
        "buscar" : {"nombre": "Tierra" },
        "campos" : {"dias":1}, //el 1 es para que filtre por el campo dias
        "limite" : 0,
        "skip": 0,
        "orden" : {"f_creacion":"desc"}
    }
```
- PUT `http://localhost:PUERTO/api/sistemasolar/planetas/identificador` siendo identificador el _id del elemento en MongoDB
- DELETE `http://localhost:PUERTO/api/sistemasolar/planetas/identificador` siendo identificador el _id del elemento en MongoDB

- Podemmos conectarnos mediante MongoDB Compass a `mongodb://82.223.81.195:6617/sistemasolar` para ver la BD.

## Guia de ficheros para modificar:
- `gema_crud_intecca.js`: Fichero principal
- `config.js`: Configuracion de datos como la db, el puerto, desarrollo/produccion...
- `app/config/middleware.js`: Se requieren las "Herramientas" y se definen las rutas
- `app/config/modelos.js`: Se definen las colecciones de la bd y los modelos
- `app/modelos`: Carpeta en la que se crean los js del modelo de cada coleccion
- `app/modsistemasolar`: Carpeta que se refiere a cada BD que tengamos, en este caso tenemos una que se llama sistemasolar. Contiene:
  - `controllers`: Carpeta que contiene dos archivos por coleccion:
      - `planetas.api.js`: Fichero en el que se definen las llamadas CRUD de la coleccion Planetas.
      - `planetas.js`: Fichero que contiene la programacion de las llamadas. Dentro se hace uso del fichero app/modbasico/basico/crud.js que es el que llama a mongodb y realiza las consultas.
  - `sistemasolar.rutas.js`: Fichero que se requiere en el middleware y contiene todas las rutas api de nuestro aplicativo
