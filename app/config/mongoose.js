'use strict'
const debug = require('debug')('gemacrudintecca:cargarBD');
const mongoose = require('mongoose');

const config = require('../../config')[process.env.NODE_ENV ||'desarrollo']

if (config.MONGO.user && config.MONGO.pass) {
	var db_addr = 'mongodb+srv://' + config.MONGO.user + ':' + config.MONGO.pass + '@cluster0.o54g6.mongodb.net/' + config.MONGO.db +'?retryWrites=true&w=majority'
} else {
	var db_addr = 'mongodb://' + config.MONGO.user + ':' + config.MONGO.pass + '@' + config.MONGO.host + ':'+config.MONGO.port+ '/' + config.MONGO.db
}
let cargarBd = function() {
	mongoose.Promise = global.Promise;	

	const db = mongoose.connect(db_addr,{useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true, useFindAndModify: false}, function (err) {
	// Fin cambio mongoose 4.13
		if (err) {
			console.trace('Mongoose connection')
			console.error(err.stack)
			process.exit(1)
		}
		debug('mongodb conexión correcta')
	})
	require('./modelos');	
	return db
}
module.exports = {
	cargarBd: cargarBd
}