'use strict'
/*
    Configuración de modelos a utilizar en el aplicativo.
*/

require('../modelos/sensor.model');	

const mongoose = require('mongoose');

const COLECCIONES = {
    Sensor:{modelo:mongoose.model('Sensor')},
};

exports.tablas = COLECCIONES;