'use strict'
const debug = require('debug')('gemacrudintecca:middleware');
const config = require('../../config')[process.env.NODE_ENV ||'desarrollo'];
// const CRUD = require('../modbasico/basico/crud');
const serveIndex = require('serve-index');
const logger = require('morgan');
const rfs = require('rotating-file-stream');

const express = require('express');

const methodOverride = require('method-override');
// const mongoose = require('mongoose');
// mongoose.set('debug', true);

module.exports = () => {
	const app = express();

	app.use(express.urlencoded({ extended: true }));
	app.use(express.json({limit:'5Mb'}));

	// LOG
	// create a rotating write stream
	const accessLogStream = rfs.createStream('access.log', {
		interval: '1d', // rotate daily
		path: `${process.cwd()}/app/public/logs`		
	});
	
	// setup the logger
	app.use(logger('combined', { stream: accessLogStream, skip: (req, res) => res.statusCode >= 400 }));
	
	const errorLogStream = rfs.createStream('error.log', {
		interval: '1d', // rotate daily
		path: `${process.cwd()}/app/public/logs`		
	});
	
	app.use(logger('combined', { stream: errorLogStream, skip: (req, res) => res.statusCode < 400 }));


	// informes públicos
	app.use("/informes", express.static(`${process.cwd()}/app/public/informes`), serveIndex('app/public/informes', { 'icons': true }));

	// logs
	app.use("/logs", express.static(`${process.cwd()}/app/public/logs`), serveIndex('app/public/logs', { 'icons': true }));

	const getDurationInMilliseconds  = (start) => {
        const NS_PER_SEC = 1e9;
        const NS_TO_MS = 1e6;
        const diff = process.hrtime(start);
    
        return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
    };

    app.use((req, res, next) => {
        //debug(`${req.method} ${req.originalUrl} ${req.body.metodo} [STARTED]`)
        const start = process.hrtime();
		
        res.on('finish', () => {            
            const durationInMilliseconds = getDurationInMilliseconds (start);
			debug(`${req.method} ${req.originalUrl} [FINISHED] ${durationInMilliseconds.toLocaleString()} ms`);
        });
    
        res.on('close', () => {
            const durationInMilliseconds = getDurationInMilliseconds (start);
			//debug(`${req.method} ${req.originalUrl} [CLOSED] ${durationInMilliseconds.toLocaleString()} ms`);			
        });
        next();
	})

	app.use(methodOverride());

	// .................................................. 
	// RUTAS

	require('../modsistemasolar/sistemasolar.rutas')(app);

	// fin RUTAS
	// .................................................. 

	app.use(function (err, req, res, next) {
		debug('middleware.err = ', err);
		if(!err.statusCode) err.statusCode = 590;
		//if(!err.statusMessage) err.statusMessage = err.message;
		//if(!err.stack)err.st
		//if(!err.coleccion) err.coleccion = 'middleware';
		if(!err.accion) err.accion = req.url;
		
		let log={
			idgcono: req.idgcono,
			role: req.role,
			coleccion: err.coleccion,
			accion: err.accion,
			ok: err.statusCode,
			//mensaje: err.statusMessage
			mensaje: err.message,
			stack: err.stack
		}
		// CRUD.grabarLog(log);
		res.status(err.statusCode).json(err)
	})
	return app;
}