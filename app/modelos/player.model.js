const mongoose = require('mongoose')
const Schema = mongoose.Schema

const playerSchema = new Schema(
	{
		nombre: String,
		descripcion: String
   	},
	{
		strict:false,
		versionKey:false,	
		timestamps: {
			createdAt: 'f_creacion',
			updatedAt: 'f_modificacion'
		}
	}
);
  
const Player = mongoose.model('Player', playerSchema)
module.exports = {Player: Player}