const mongoose = require('mongoose')
const Schema = mongoose.Schema

const sensorSchema = new Schema(
	{
		sensor_name: String,
		serial: Number,
		description: String,
		data_type: String,
		reading_rate: String
   	},
	{
		strict:false,
		versionKey:false,	
		timestamps: {
			createdAt: 'f_creacion',
			updatedAt: 'f_modificacion'
		}
	}
);
  
const Sensor = mongoose.model('Sensor', sensorSchema)
module.exports = {Sensor: Sensor}