const mongoose = require('mongoose')
const Schema = mongoose.Schema

const planetaSchema = new Schema(
	{
		nombre: String,
		descripcion: String
   	},
	{
		strict:false,
		versionKey:false,	
		timestamps: {
			createdAt: 'f_creacion',
			updatedAt: 'f_modificacion'
		}
	}
);
  
const Planeta = mongoose.model('Planeta', planetaSchema)
module.exports = {Planeta: Planeta}