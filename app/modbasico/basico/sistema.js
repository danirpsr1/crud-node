'use strict'
var {exec} = require("child_process");

module.exports = {
    runComandos:runComandos
}

/**
 * @function runComando
 * @description Ejecuta un comando exec de child_process.
 * @memberOf modbasico/basico/sistema
 * @param {String} comando
 * @return {Object} {comando:commandDescription, salida:finsalida}
 * 
* */

function runComando(comando) {
    let promesa = (resolve,reject) =>{        
        exec(comando, (execError, stdout, stderr) => {
            var comandoDescripcion = JSON.stringify(comando);
            if (execError && execError.code === 127) {
                reject({notfound: true})
            }
            else if (execError) {
                var runError = new Error("Command failed: " + comandoDescripcion);
                if (stderr) runError.stderr = stderr.trim();
                if (execError) runError.execError = execError;

                reject({error: runError})
            }
            else {
                let salida= stdout.toString().split('\n');
                var finsalida='';
                for(var i=0; i<salida.length;i++){
                    if(salida[i]!='') finsalida=salida[i]
                }
                resolve({comando:comandoDescripcion, salida:finsalida})
            }
        });
    }
    return new Promise(promesa)
}

/**
 * @function runComandos
 * @description Promise.all de runComando(comando)
 * @memberOf modbasico/basico/sistema
 * @return {Object} {data}
 * 
* */
function runComandos(comandos) {
    let promesa = (resolve,reject) => {
        var promesaComandos = [];
        for(var registro in comandos){ 
            promesaComandos.push(runComando(comandos[registro]));
        }
        Promise.all(promesaComandos)    
            .then(data => resolve(data))
            .catch(err => reject(err))
    }
    return new Promise(promesa)
}