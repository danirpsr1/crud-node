const debug = require('debug')('gema_intecca:serviciohttp');

const http = require("http");
const https = require("https");
const url_obj = require("url");

const servicioHttp = async (url_string, method = "GET", postData) => {
  const url = url_obj.parse(url_string);
  const lib = url.protocol=="https:" ? https : http;

  debug(`url ${JSON.stringify(url)}`)
  debug(`body: ${JSON.stringify(postData)}`)
 
  const params = {
    method:method,
    hostname:url.hostname,
    auth:url.auth,
    json:true,
    rejectUnauthorized: false,
    //port: url.port || url.protocol=="https:" ? 443 : 80,
    port: url.port || 80,
    path: url.path || "/",
    headers: { 
      "Content-Type": "application/json",
      }
  };

  debug(`params: ${JSON.stringify(params)}`)

  return new Promise((resolve, reject) => {
    const req = lib.request(params, res => {

      debug('Headers: ' + JSON.stringify(res.headers));

      res.setEncoding('utf8');

      // if (res.statusCode < 200 || res.statusCode >= 300) {
      //   //return reject(new Error(`Status Code: ${res}`));
      //   resolve(new Error(`Status Code: ${res}`));
      // }
      const datos = [];

      //res.on("data", chunk => datos.push(chunk));
      res.on("data", chunk => datos.push(Buffer.from(chunk)));
 
      res.on("end", () => {
        let body = Buffer.concat(datos).toString('utf8');
            switch(res.headers['content-type']) {
                case 'application/json':
                    body = JSON.parse(body);
                    break;
            }
        resolve(JSON.parse(datos))
      });
    });

    if (postData) {
      //req.write(""+postData);
      req.write(JSON.stringify(postData),encoding='utf8');
    }
    // req.on("socket", (err) =>{
    //   debug.log('------ socket ------------------')
    //   resolve(err)
    // });

    req.on("error", (err) =>{
      debug('-------------- errorrrrrrrrrrr');
      reject(err)
    });

    // req.on("close", (err) =>{
    //   debug('------ close ------------------');
    //   //resolve(err)
    // });

    req.end();
  });
}

exports.servicioHttp = servicioHttp;
