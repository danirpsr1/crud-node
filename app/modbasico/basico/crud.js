/**
 * DEPRECATED
 * save() -->MongoDB deprecates the db.collection.save() method. Instead use db.collection.insertOne() or db.collection.replaceOne() instead.
 * 
 */
'use strict'
const {tablas} = require('../../config/modelos')

const mongoId = require('mongodb').ObjectID;

module.exports = {
    leerCampo:leerCampo,
    leerId:leerId,
    nuevo:nuevo,
    modificarId:modificarId,
    modificarUno: modificarUno,
    borrar:borrar,
}

function leerCampo(opciones, tabla){
    let promesa = (resolve,reject) =>{
        let buscar = {};
        let campos = {};
        let orden = {};
        let limite = 0;

        if(opciones.hasOwnProperty('buscar') && opciones.buscar != null && opciones.buscar !='') buscar = opciones.buscar;
        if(opciones.hasOwnProperty('orden') && opciones.orden != null && opciones.orden !='') orden = opciones.orden;
        if(opciones.hasOwnProperty('campos') && opciones.campos != null && opciones.campos !='') campos = opciones.campos;
        if(opciones.hasOwnProperty('limite') && opciones.limite != null && opciones.limite !='') limite = opciones.limite;

        const MODELO = tablas[tabla].modelo
        if(limite== 1){
            MODELO.findOne(buscar).lean()
                .sort(orden)
                .select(campos)
                .then(registro => {
                    if (!registro) resolve({ok:false, mensaje:tabla+'. Lectura: No encontrado búsqueda= '+ JSON.stringify(opciones.buscar),datos:''});
                    resolve({ok:true, mensaje:tabla, datos:registro})
                })
                .catch(err => {
                    err.coleccion = tabla;
                    err.accion = 'leerCampo';
                    reject(err)
                });
        }else{
            MODELO.find(buscar).lean()
                .sort(orden)
                .limit(limite)
                .select(campos)
                .then(registros => {
                    if (registros.length<1) resolve({ok:false, mensaje:tabla+'. Lectura: No encontrado búsqueda= '+ JSON.stringify(opciones.buscar),datos:''})
                    else resolve({ok:true, mensaje:tabla, contar:registros.length, datos:registros})
                })
                .catch(err => {
                    err.coleccion = tabla;
                    err.accion = 'leerCampo';
                    reject(err)
                });
        }  
    }
    return new Promise(promesa)
}

function leerId(id, tabla){
    let promesa = (resolve,reject) =>{
        if(mongoId.isValid(id)){
            const MODELO = tablas[tabla].modelo;
            MODELO.findById(id).lean()
                .then(registro =>{
                    if (!registro) reject({ok:false, mensaje:'Colección: '+tabla+'. No existe el registro id='+id,datos:''})
                    else resolve({ok:true, mensaje:tabla, datos:registro})})
                .catch(err => {
                    err.coleccion = tabla;
                    err.accion = 'leerId';
                    reject(err)
                })
        }else reject({ok:false, mensaje:tabla+".ObjectId: "+id+" inválido", datos:''})
    }
    return new Promise(promesa)
}


function nuevo(reg, tabla){    
    let promesa = (resolve,reject) =>{
        const MODELO = tablas[tabla].modelo;
        MODELO.create(reg, function (err, nuevoreg) {
            if (err){
                err.coleccion = tabla;
                err.accion = accion;
                reject(err)
            };
            resolve({
                ok:true, 
               // mensaje: tabla + '. Nuevo registro creado. id= '+ nuevoreg._id,
                datos: nuevoreg
            }); 
        })        
    }
    return new Promise(promesa)
}

function modificarId(id, reg, tabla){
    let promesa = (resolve,reject) =>{
        const MODELO = tablas[tabla].modelo;
        delete reg._id;
        // borro f_modificacion para que actualize la fecha de modificación, pues no lo hace automáticamente, aunque está así configurado en el esquema
        delete reg.f_modificacion;
        
        MODELO.findOneAndUpdate({_id:id}, reg, {new:true},function (err, registro) {  
            if (err){
                err.coleccion = tabla;
                err.accion = accion;
                reject(err)
            }else if (!registro){
                const err = new Error('Error no existe el registro id= '+id);
                err.coleccion = tabla;
                err.accion = accion;
                reject(err)
            };
            resolve({ok:true, mensaje:tabla+'. Modificado registro id='+id, datos:registro});
        })   
    }
    return new Promise(promesa)
}


function modificarUno(buscar, reg, tabla){
    let promesa = (resolve,reject) =>{
        const MODELO = tablas[tabla].modelo;
        delete reg._id;
        // borro f_modificacion para que actualize la fecha de modificación, pues no lo hace automáticamente, aunque está así configurado en el esquema
        delete reg.f_modificacion;
        MODELO.findOneAndUpdate(buscar, reg, {new:true}, function (err, registro) {
            if (err){
                err.coleccion = tabla;
                err.accion = 'modificar';
                reject(err)
            }else if (!registro){
                reject({ok:false, message: tabla + '. Modificar: No se encuentran registros en la búsqueda'+ JSON.stringify(buscar), coleccion: tabla, accion:'modificar'})
            };
            resolve({ok:true, mensaje:tabla+'. Modificado en búsqueda= '+buscar, datos:registro})
        })   
    }
    return new Promise(promesa)
}


function borrar(id, tabla){
    let promesa = (resolve,reject) =>{
        const MODELO = tablas[tabla].modelo;
        MODELO.deleteOne({_id:id},function (err, result){
            if (err) {
                err.coleccion = tabla;
                err.accion = 'borrar';
                reject(err)
            }else if(result.deletedCount < 1){
                resolve({ok:false, mensaje: tabla + '. Borrado: No encontrado id= '+ id, datos:result})
            };
            resolve({ok:true, mensaje: tabla+'. Registro borrado', datos:result})
        })
    }
    return new Promise(promesa)
}
