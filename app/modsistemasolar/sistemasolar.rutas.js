'use strict'
const planetas_api = require('./controllers/planetas.api.js');

module.exports = (app) => {    
    planetas_api(app, '/api/sensors-api/sensors')
}