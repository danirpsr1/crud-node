﻿'use strict'

const TABLA = 'Sensor';
const CRUD = require('../../modbasico/basico/crud.js');

module.exports = {
    leerCampo:leerCampo,
    leerId:leerId,
    nuevo:nuevo,
    modificar:modificar,
    borrar:borrar
}


function leerCampo(opciones){
    let promesa = (resolve,reject) =>{
        CRUD.leerCampo(opciones, TABLA)
            .then(result => resolve(result))
            .catch(err => reject(err))
    }
    return new Promise(promesa)
}


function leerId(id){
    let promesa = (resolve,reject) =>{
        let opciones = {
            buscar : {_id:id},
            campos : {},            
            limite : 1,
            orden : {}
        };
        CRUD.leerCampo(opciones, TABLA)
            .then(result => resolve(result))
            .catch(err => reject(err))
    }
    return new Promise(promesa)
}


function nuevo(reg){
    let promesa = (resolve,reject) =>{
        CRUD.nuevo(reg, TABLA)
            .then(nuevoRegistro => resolve(nuevoRegistro))
            .catch(err => reject(err))
    }
    return new Promise(promesa)
}


function modificar(id, reg){
    let promesa = (resolve, reject) =>{
        CRUD.modificarId(id, reg, TABLA)
            .then(resModificarId => resolve(resModificarId))
            .catch(err => reject(err))
    }
    return new Promise(promesa)
}


function borrar(id){
    let promesa = (resolve,reject) =>{
        CRUD.borrar(id, TABLA)
            .then(resBorrarTrabajo =>  resolve(resBorrarTrabajo))
            .catch(err =>  reject(err))
    }
    return new Promise(promesa)

}

