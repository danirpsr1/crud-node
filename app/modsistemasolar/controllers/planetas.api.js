'use strict'
const debug = require('debug')('gemacrudintecca:planetas.api');
const PLANETAS = require('./planetas.js');

module.exports = (app, ruta) => {

    app.route(ruta)
        .get((req, res, next) => {
            let opciones = {
                buscar : {},
                campos : {},
                limite : 0,
                orden : {'f_modificacion':'desc'}
            };
            PLANETAS.leerCampo(opciones)
                .then(result => res.status(200).json(result))
                .catch(err => next(err))
        })

    app.route(`${ruta}/:id`)
        .get((req, res, next) => {
            PLANETAS.leerId(req.params.id)
                .then(result => res.status(200).json(result))
                .catch(err => next(err))
        }) 

         
    app.route(ruta)
        .post((req, res,next) => {
            PLANETAS.nuevo(req.body)
                .then(result => res.status(201).json(result))
                .catch(err => next(err))
        })


    app.route(`${ruta}/:id`)
        .put((req, res,next) => {
            PLANETAS.modificar(req.params.id, req.body)
                .then(result => res.status(200).json(result))
                .catch(err => next(err))
        })        


    app.route(`${ruta}/:id`)
        .delete((req, res, next) => {
            PLANETAS.borrar(req.params.id)
                .then(result => res.status(200).json(result))
                .catch(err => next(err))
        })
   
    app.route(`${ruta}/query`)
        .post((req, res, next) => {
            let opciones = {
                coleccion: '',
                buscar : {},
                campos : {},
                limite : 0,
                skip: 0,
                orden : {'f_creacion':'desc'}
            };
            if(req.body.buscar) opciones.buscar = req.body.buscar;
            if(req.body.limite) opciones.limite = req.body.limite;
            if(req.body.skip) opciones.skip = req.body.skip;
            if(req.body.campos) opciones.campos = req.body.campos;
            if(req.body.orden) opciones.orden = req.body.orden;
            PLANETAS.leerCampo(opciones)
                .then(result => res.status(200).json(result))
                .catch(err => next(err))
        }) 
        
}